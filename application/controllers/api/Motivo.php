<?php
include_once APPPATH . 'controllers/api/API_Controller.php';

class Motivo extends API_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('api/MotivoModel');
		try {
			$this->displayJSON($this->MotivoModel->lista());
		} catch (Exception $exception) {
			$this->displayJSON(["message" => "Ocorreu uma falha interna. Por favor tente novamente mais tarde."]);
		}
	}
}
