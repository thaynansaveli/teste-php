<?php

class API_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	protected function displayJSON($json)
	{
		$str = json_encode($json, JSON_NUMERIC_CHECK);
		$str = str_replace('\\/', '/', $str);
		$str = preg_replace_callback(
			'/\\\\u([0-9a-f]{4})/i',
			function ($matches) {
				$sym = mb_convert_encoding(
					pack('H*', $matches[1]),
					'UTF-8',
					'UTF-16'
				);

				return $sym;
			},
			$str
		);

		header("Content-type: application/json; charset=utf-8");
		echo $str;
	}
}
